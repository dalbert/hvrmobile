import {
	AppRegistry,
} from 'react-native';
import hvrMobile from 'hvrMobile/app/App';

AppRegistry.registerComponent('hvrMobile', () => hvrMobile);
