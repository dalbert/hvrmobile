import React from "react";
import {StyleSheet} from "react-native";
import {COLORS, GLOBALCONSTANTS} from "./Constants";

var globalStyles = StyleSheet.create({
    buttonPrimaryContainer: {
        backgroundColor: COLORS.WHITE,
        padding: 15,
        borderRadius: GLOBALCONSTANTS.BORDERRADIUS
    },
    buttonPrimaryText: {
        color: COLORS.PUBLIC,
        textAlign: 'center',
        fontSize: 14
    },
    buttonSecondaryContainer: {

    },
    buttonSecondaryText: {

    },
    mainContainer: {
        paddingLeft: 20,
        paddingRight: 20,
    },
    textInputContainer: {
        borderWidth: 1,
        borderColor: COLORS.WHITE,
        paddingLeft: 8,
        paddingRight: 8,
        marginTop: 10,
        borderRadius: GLOBALCONSTANTS.BORDERRADIUS,
        flexDirection: 'row',
        alignItems: 'center',
    },
    errorMessageBox: {
        borderRadius: 3,
        backgroundColor: COLORS.ERROR,
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    errorMessageTop: {
        width: 0,
        height: 0,
        borderTopWidth: 0,
        borderRightWidth: 5,
        borderLeftWidth: 5,
        borderBottomWidth: 8,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'crimson',
        borderLeftColor: 'transparent',
        position: 'absolute',
        top: -8,
        left: 20,
    },
    passwordInputVisibility: {
        width: 20,
        resizeMode: 'contain',
    },
    textInput: {
        color: '#FFFFFF',
    },
    title: {
        fontSize: 24
    },
    link: {
        textDecorationLine: 'underline'
    },
    errorText: {
        color: COLORS.ERROR,
    },
    errorContainer: {
        borderColor: COLORS.ERROR,
    },
    background: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
    },
    test: {
        borderColor: 'crimson',
        borderWidth: 1,
    }

});

export default globalStyles