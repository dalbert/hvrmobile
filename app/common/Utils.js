import {COLORS, COMMENT_TYPE} from "./Constants";
import moment from "moment";

function _getColor(commentType){
    if (COMMENT_TYPE.PUBLIC === commentType){
        return COLORS.PUBLIC;
    }else if (COMMENT_TYPE.PRIVATE === commentType){
        return COLORS.PRIVATE;
    }else if (COMMENT_TYPE.GROUP === commentType){
        return COLORS.GROUP;
    }
}

const Utils = {

    tagObjectToWebViewTag(tag){
        var newTag = tag.id + '::' + tag.hash + '::' + tag.commentType;

        return newTag;
    },
    getColor(commentType){
        return {color: _getColor(commentType)};
    },
    getBackgroundColor(commentType){
        return {backgroundColor: _getColor(commentType)};
    },
    getBorderColor(commentType){
        return {borderColor: _getColor(commentType)};
    },
    getTimeElapsed(date){
        let previousDate = moment(date);
        let timeElapsedInMilis = moment().diff(previousDate);
        let duration = moment.duration(timeElapsedInMilis);

        return duration.humanize() + ' ago';
    }
};

export default Utils;
