const COMMENT_TYPE = {
    PUBLIC: 'PUBLIC',
    PRIVATE: 'PRIVATE',
    GROUP: 'GROUP',
};

const COLORS = {
    PUBLIC: '#27AE60', //'rgb(39, 174, 96)',
    PRIVATE: 'rgba(0, 154, 255, 0.972549)',
    GROUP: 'rgb(214, 117, 214)',
    WHITE: 'rgb(255,255,255)',
    BLACK: 'rgb(0,0,0,)',
    DARKGREY: 'rgb(34,34,34)',
    GREY: 'rgb(102,102,102)',
    LIGHTGREY: 'rgb(204,204,204)',
    ERROR: '#D0021B',
};

const GLOBALCONSTANTS = {
    BORDERRADIUS: 5
};

const REGEX = {
    VALID: {
        EMAIL: /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
        USERNAME: /.{3,30}/,
        PASSWORD: /.{3,30}/
    },
};

const STATUS = {
    AVAILABLE: 0,
    INUSE: 1,
    INVALID: 2,
};


export {COLORS, COMMENT_TYPE, GLOBALCONSTANTS, REGEX, STATUS};
