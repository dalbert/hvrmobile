const images = {
	icons : {
        logo: {
            login: require('../images/Hvr_logo-login.png'),
            landing: require('../images/Hvr_logo-landing.png'),
        },
        eye: {
            hidden: require('../images/eye-hidden.png'),
            shown: require('../images/eye-shown.png'),
        }
	},
	buttons:{
		eye: require('../images/Eye.png'),
		feed: require('../images/Feed.png'),
		message: require('../images/Message.png'),
		newPin: require('../images/NewPin.png'),
		profile: require('../images/Profile-icon.png'),
		radial: require('../images/Hvr-button.png'),
		search: require('../images/Search.png'),
    },
    backgrounds: {
        world: require('../images/Background_Image-world.png'),
	}
};

export default images;