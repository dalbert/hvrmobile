import React from "react";
import {StyleSheet} from "react-native";
import {COLORS, GLOBALSTYLE} from "hvrMobile/app/common/Constants";

var signUpStyles = StyleSheet.create({
    title: {
        textAlign: 'center',
        color: COLORS.DARKGREY,
        marginBottom: 10
    },
    subTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: COLORS.GREY,
        textAlign: 'center',
        marginBottom: 10
    },
    instructions: {
        color: COLORS.LIGHTGREY,
        textAlign: 'center'
    },
    link: {
        color: COLORS.WHITE
    }
});

export default signUpStyles