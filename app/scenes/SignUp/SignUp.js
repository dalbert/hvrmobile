import React, {Component} from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import images from "hvrMobile/app/config/images";
import GLOBALSTYLES from "hvrMobile/app/common/globalStyles";
import {COLORS, REGEX, STATUS} from "hvrMobile/app/common/Constants";
import loginStyles from "hvrMobile/app/scenes/Login/styles";
import styles from "./styles";
import session from "hvrMobile/app/services/session";
import FormInput from "hvrMobile/app/components/FormInput";

const ERROR_MESSAGE = {
    EMAIL: {
        INVALID: 'Email must be a valid email',
        INUSE: 'Email already in use',
        REQUIRED: 'This field is required',
    },
    USERNAME: {
        INVALID: 'Username length must be between 3 and 30 characters',
        INUSE: 'Login already in use',
        REQUIRED: 'This field is required',
    },
    PASSWORD: {
        INVALID: 'Password length must be between 3 and 30 characters',
    }
};

class signUp extends Component {

    validateEmail(isValid) {

        console.log('signup - validateEmail - ', isValid);

        let promise = new Promise((resolve, reject) => {

            if (REGEX.VALID.EMAIL.test(this.state.email)) {
                // console.log('valid email format');
                // return session
                session.validateEmail(this.state.email)
                    .then((response) => {
                        if (response.status === 200) {
                            console.log('email promise 200: ');
                            this.setState({emailMessage: ''});
                            resolve(isValid && true);
                        } else {
                            console.log('email promise else: ');
                            this.setState({emailMessage: ERROR_MESSAGE.EMAIL.INUSE});
                            resolve(false);

                        }
                    })
                    .catch((error) => {
                        console.log('email validation error:', error);
                        reject();
                    });
            } else {
                this.setState({emailMessage: ERROR_MESSAGE.EMAIL.INVALID});
                resolve(false);
            }
        });

        return promise;
    }

    validateUsername(isValid) {
        console.log('signup - validateUsername - ', isValid);

        let promise = new Promise((resolve, reject) => {
            console.log('signup - promise 000');
            console.log('signup promise this.stat: ', this.state);

            if (REGEX.VALID.USERNAME.test(this.state.username)) {
                console.log('username matched regex');
                session.validateUsername(this.state.username)
                    .then((response) => {

                        if (response.status === 200) {
                            console.log('signup - promise - 200');
                            this.setState({usernameMessage: ''});
                            resolve(isValid && true);
                        } else {
                            console.log('signup - promise - else');
                            this.setState({usernameMessage: ERROR_MESSAGE.USERNAME.INUSE});
                            resolve(false);
                        }
                    })
                    .catch((error) => {
                        console.log('system error username:', error);
                        reject();
                    });
            } else {
                console.log('username not matching regex');
                this.setState({usernameMessage: ERROR_MESSAGE.USERNAME.INVALID});
                resolve(false);
            }
        });

        return promise;
    }

    _resetEmailValidation() {
        console.log('resetting email validation');
        this.setState({emailMessage: ''});
        this.setState({validEmail: false});
    }

    _resetUsernameValidation() {
        console.log('resetting username validation');
        this.setState({usernameMessage: ''});
        this.setState({validUsername: false});
    }

    _resetPasswordValidation() {
        console.log('resetting password validation');
        this.setState({passwordMessage: ''});
    }


    validatePassword(isValid) {

        console.log('validatePassword - ', isValid);

        let promise = new Promise((resolve, reject) => {

            if (REGEX.VALID.PASSWORD.test(this.state.password)) {
                this.setState({passwordMessage: ''});
                resolve(isValid && true);
            } else {
                this.setState({passwordMessage: ERROR_MESSAGE.PASSWORD.INVALID});
                resolve(false);
            }
        });

        return promise;
    }

    validateInputs() {
        return this.validateEmail(true)
            .then(this.validateUsername.bind(this))
            .then(this.validatePassword.bind(this));
    }

    doSignUp() {
        console.log('doing signup');
        session.signup(this.state.email, this.state.username, this.state.password)
            .then(() => {
                console.log('succesfully signed up');
                this.props.route.passProps.signupSuccess();
                this.props.navigator.pop();
            })
            .catch(() => {
                console.log('error on signing up');
            });

    }

    _onPressSignUp() {
        // reset page-level errors
        console.log('pressing sign up');

        this.validateInputs()
            .then((result) => {
                console.log('_onPressSignUp: ', result);
                if (result) {
                    this.doSignUp();
                } else {
                    console.log('on press signup: not valid');
                }
            })
            .catch((error) => {
                console.log('error system?:', error);
            });

    }

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            username: '',
            password: '',
            emailMessage: '',
            usernameMessage: '',
            passwordMessage: '',
            pageErrorMessage: '',
        };
    }

    _renderEmailMessage() {
        if (this.state.emailMessage.length === 0) {
            return null;
        }
        return (<Text style={GLOBALSTYLES.errorText}>{this.state.emailMessage}</Text>);
    }

    _renderUsernameMessage() {
        if (this.state.usernameMessage.length === 0) {
            return null;
        }
        return (<Text style={GLOBALSTYLES.errorText}>{this.state.usernameMessage}</Text>);
    }

    _renderPasswordMessage() {
        if (this.state.passwordMessage.length === 0) {
            return null;
        }
        return (<Text style={GLOBALSTYLES.errorText}>{this.state.passwordMessage}</Text>);
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={[GLOBALSTYLES.background, {backgroundColor: '#299155'}]}>
                    <Image source={images.backgrounds.world} style={[GLOBALSTYLES.background]}/>
                </View>
                <View style={[GLOBALSTYLES.mainContainer, GLOBALSTYLES.background]}>
                    <View style={[{zIndex: 103}, {flexDirection: 'column', flex: 1}]}>
                        <View style={[{flex: 1, justifyContent: 'center', alignItems: 'center'}]}>
                            <Image style={[loginStyles.logo]}
                                   source={images.icons.logo.login}
                            />
                        </View>
                        <View style={[{flex: 2}]}>
                            <FormInput
                                keyboardType='email-address'
                                hasError={this.state.emailMessage.length > 0}
                                value={this.state.email}
                                placeholder='Email'
                                errorMessage={this.state.emailMessage}
                                onChange={() => this._resetEmailValidation()}
                                onChangeText={(email) => this.setState({email})}></FormInput>


                            <FormInput
                                hasError={this.state.usernameMessage.length > 0}
                                value={this.state.username}
                                placeholder='Create a Username'
                                errorMessage={this.state.usernameMessage}
                                onChange={() => this._resetUsernameValidation()}
                                onChangeText={(username) => this.setState({username})}></FormInput>

                            <FormInput
                                hasError={this.state.passwordMessage.length > 0}
                                value={this.state.password}
                                placeholder='Password'
                                isPassword={true}
                                errorMessage={this.state.passwordMessage}
                                onChange={() => this._resetPasswordValidation()}
                                onChangeText={(password) => this.setState({password})}
                            />

                            <TouchableOpacity onPress={() => this._onPressSignUp()} style={{marginTop: 10}}>
                                <View style={loginStyles.loginButtonContainer}>
                                    <Text style={loginStyles.loginButtonText}>SIGN UP</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{marginTop: 20}}>
                                <Text onPress={() => this.props.navigator.pop()}
                                      style={[GLOBALSTYLES.link, styles.link]}>Already have an account? Sign In!</Text>
                            </View>
                        </View>
                    </View>

                </View>
            </View>

        )
    }
}

export default signUp