import React from "react";
import {StyleSheet} from "react-native";
import {COLORS} from "hvrMobile/app/common/Constants";

var mainStyle = StyleSheet.create({
    webContainer: {
        flex: 1,
        flexDirection: 'column'
    },
    container: {
        flex:1,
        flexDirection: 'column',
        borderWidth: 0,
        borderColor: 'blue',
    },
    topNav: {
        flexDirection: 'row',
        alignItems:'center',
        borderColor: '#ECF0F1',
        borderWidth: 1,
    },
    topNavItem: {

    },
    urlInput: {
        flex: 0.6,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        marginTop: 5,
        marginBottom: 5,
    },
    cancelWriteCommentButton: {
        position: 'absolute',
        right: 10,
        top: 10,
        zIndex: 104,
        alignItems: 'center',
        width: 20,
        height: 20,
    },
    hvrActionButtonContainer: {
        position: 'absolute',
        height: 70,
        width: 70,
        right: 40,
        bottom: 40,
    },
    ButtonRadialContainer:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 101,
    },
    ButtonRadial:{
        resizeMode: 'contain',
        height: 70,
        width: 70,
    },
    ButtonSubRadialContainer:{
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 100,
        borderWidth: 0,
        borderColor: 'crimson',
    },
    hvrActionButtonSubRadial: {
        resizeMode: 'contain',
        height: 50,
        width: 50,
    },
    visibleCommentsList: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        marginRight: 115,
        marginLeft: 5,
        maxHeight: 200,
        padding: 0,
        justifyContent: 'flex-end',
    },
    commentListItemContainer:{
        height: 50,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#95A5A6',
        backgroundColor: '#FFFFFF',
        marginBottom: 5,
        flexDirection: 'row',
        elevation: 4, // android
        shadowColor: 'crimson', // IOS
        alignItems: 'center',
    },
    commentTypeIndicator: {
        height: 48,
        width: 48,
        borderRadius: 24,
        borderWidth: 5,
        marginRight: 10,
    },
    commentPublicIndicator: {

    },
    commentData: {
        flexDirection: 'column',
        borderTopWidth: 5,
        borderBottomWidth: 5,
    },
    inputContainerBackground:{
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 102,
        alignItems: 'stretch',
        flexDirection: 'column',
    },
    inputMask:{
        position: 'absolute',
        bottom: 0,
        top: 0,
        right: 0,
        left: 0,
        backgroundColor: '#000000',
        opacity: 0.3,
        alignSelf: 'stretch',
        zIndex: 103,
    },
    inputContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,

        backgroundColor: 'yellow',
        zIndex: 104,
    },
    commentInput:{
        backgroundColor: '#FFFFFF',
        borderTopWidth: 1,
        borderColor: '#DDDDDD',
        color: '#34495E',
        height: 100,
    },
    commentTypeContainer: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: '#ECF0F1',
    },
    commnetTypeButtonInnerContainer: {
        marginVertical: 10,
        flex: 1,
        alignItems: 'center',
    },
    postButtonContainer: {
        flex: 2,
        paddingRight: 20,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    postButton: {
        backgroundColor: '#27AE60',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
        right:0,
        textAlign: 'center',
        color: '#FFFFFF',
        marginVertical: 2,
    },
    privateType:{
        backgroundColor: COLORS.PRIVATE,
    },
    privateTextType:{
        color: COLORS.PRIVATE,
    },
    publicType:{
        backgroundColor: COLORS.PUBLIC,
    },
    publicTextType:{
        color: COLORS.PUBLIC,
    },
    groupType: {
        backgroundColor: COLORS.GROUP,
    },
    groupTextType: {
        color: COLORS.GROUP,
    },
    radioButton:{
        height: 17,
        width: 17,
        borderRadius: 8,
    },
    inactiveType:{
        backgroundColor: '#95A5A6',
    },
    inactiveTextType: {
        color: '#95A5A6',
    }

});

export default mainStyle;