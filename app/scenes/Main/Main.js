import React, {Component} from "react";
import {
    Animated,
    Image,
    InteractionManager,
    Keyboard,
    ListView,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableWithoutFeedback,
    View
} from "react-native";
import WebViewBridge from "hvrMobile/app/components/webviewbridge";
import styles from "./styles";
import images from "hvrMobile/app/config/images";
import CryptoJS from "crypto-js";

import session from "hvrMobile/app/services/session";
import Utils from "hvrMobile/app/common/Utils";
import {COMMENT_TYPE} from "hvrMobile/app/common/Constants";
const _ = require('lodash');
const HVR_ACTION = {
    SHOW_VISIBLE_TAGS: 'showVisibleTags',
    DEBUG: 'Debug',
    UPDATE_TAGLIST: 'updateTagList',
    GET_TEXT_HASH: 'getTextHash',
    GET_IMAGE_HASH: 'getImageHash',
    WRITE_COMMENT: 'writeComment',
    SET_PAGE_DATA: 'setPageData',
};

const TAG_MODE = {
    IDLE: 0,
    SELECT: 1,
    WRITE: 2,
};

const INJECTED_JS_METHODS= {
    setTextHash: 'setTextHash',
    setImageHash: 'setImageHash',
    doShowTags: 'doShowTags',
};

class Main extends Component {

    static propTypes = {};

    static defaultProps = {};

    componentWillMount(){
        this.keyboardDidShowListenr = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListenr = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
        Keyboard.dismiss();
    }

    _keyboardDidShow(){
        console.log('keyboard shown');
        this.setState({isKeyboardOpen: true});
    }

    _keyboardDidHide(){
        console.log('keyboard hid');
        this.setState({isKeyboardOpen: false});
    }

    constructor(props) {
        super(props);

        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            isKeyboardOpen: false,
            tagMode: TAG_MODE.IDLE,
            url: '',
            tagList: [],
            visibleTags: [],
            ds: ds,
            dsVisibleCommments : ds.cloneWithRows([]),
            progress: 0,
            inputUrl: '',
            isOpenHvrAction: false,
            isShowComments: false,
            renderPlaceholderOnly: true,
            initRight: 50,
            initBottom: 50,
            searchRight: new Animated.Value(45),
            searchBottom: new Animated.Value(45),
            eyeRight: new Animated.Value(45),
            eyeBottom: new Animated.Value(45),
            messageRight: new Animated.Value(45),
            messageBottom: new Animated.Value(45),
            newTagRight: new Animated.Value(45),
            newTagBottom: new Animated.Value(45),
            dx: 40,
            dy: 40,
            comment: null,
            selectedCommentType: COMMENT_TYPE.PUBLIC,
            buttonPublicStyle: styles.publicType,
            buttonPublicTextStyle: styles.privateTextType,
            buttonPrivateStyle: styles.inactiveType,
            buttonPrivateTextStyle: styles.inactiveTextType,
            buttonGroupStyle: styles.inactiveType,
            buttonGroupTextStyle: styles.inactiveTextType,
            siteUrl: '',
            ogTitle: '',
            commentingData: {},
        };
    }

    toggleComments(){
        console.log('toggleComments: ' + this.state.isShowComments);
        if (!this.state.isShowComments){
            this.doShowTags();
        }
        this.setState({isShowComments: !this.state.isShowComments});
    }

    convertToWebViewTagList(tagList){
        var webViewTagList = [];
        for (var i=0; i < tagList.length; i++){
            webViewTagList.push(Utils.tagObjectToWebViewTag(tagList[i]));
        }
        return webViewTagList;
    }

    onPressPrivateButton(){
        console.log('press private');
        this.selectCommentType(COMMENT_TYPE.PRIVATE);
    }

    onPressPublicButton(){
        console.log('press public');
        this.selectCommentType(COMMENT_TYPE.PUBLIC);
    }

    onPressGroupButton(){
        console.log('press group');
        this.selectCommentType(COMMENT_TYPE.GROUP);
    }

    selectCommentType(commentType){
        console.log('selecting comment type: ', commentType);
        this.setState({buttonPublicStyle :  commentType === COMMENT_TYPE.PUBLIC ? styles.publicType:styles.inactiveType});
        this.setState({buttonPublicTextStyle:  commentType === COMMENT_TYPE.PUBLIC ? styles.publicTextType:styles.inactiveTextType});
        this.setState({buttonPrivateStyle: commentType === COMMENT_TYPE.PRIVATE ? styles.privateType:styles.inactiveType});
        this.setState({buttonPrivateTextStyle: commentType === COMMENT_TYPE.PRIVATE ? styles.privateTextType:styles.inactiveTextType});
        this.setState({buttonGroupStyle: commentType === COMMENT_TYPE.GROUP ? styles.groupType:styles.inactiveType});
        this.setState({buttonGroupTextStyle: commentType === COMMENT_TYPE.GROUP ? styles.groupTextType:styles.inactiveTextType});
        this.setState({selectedCommentType: commentType});
    }

    postComment(){
        console.log('posting comment');
    }

    sendTextHashToWebView(contentString){
        var tokens = contentString.split(/\|(.+)/);
        var id = tokens[0];

        var textContent = decodeURIComponent(tokens[1]);
        var textHash = CryptoJS.MD5(textContent).toString();

        // need to save these hashes for future

        // console.log('commandString: ' + INJECTED_JS_METHODS.setTextHash + '(' + id + ', "' + textHash + '");');
        this.refs.webviewbridge.runJavaScript(INJECTED_JS_METHODS.setTextHash + '(' + id + ', "' + textHash + '");');
    }

    doShowTags(){
        this.refs.webviewbridge.runJavaScript(INJECTED_JS_METHODS.doShowTags + '();');
    }

    sendImageHashToWebView(contentString){
        var tokens = contentString.split(/\|(.+)/);
        var id = tokens[0];

        var imageSrc = decodeURIComponent(tokens[1]);
        var imageHash = CryptoJS.MD5(imageSrc).toString();
        // this will need to call pHash, then check closest matching phash for image element hashes

        // console.log('commandString: ' + INJECTED_JS_METHODS.setImageHash + '(' + id + ', "' + imageHash + '");');
        this.refs.webviewbridge.runJavaScript(INJECTED_JS_METHODS.setImageHash + '(' + id + ', "' + imageHash + '");');
    }

    exitHuvrTagMode(){
        this.refs.webviewbridge.runJavaScript('exitHuvrTagMode();');
        this.setState({tagMode: TAG_MODE.IDLE});
    }

    onPressTagButton(){
        if (this.state.tagMode === TAG_MODE.IDLE){
            this.refs.webviewbridge.runJavaScript('enterHuvrTagMode();');
            this.setState({tagMode: TAG_MODE.SELECT});
        }else{
            this.exitHuvrTagMode();

        }
    }

    showComments(tags){

        let {tagList, visibleComments} = this.state;
        let tempTags = tags.split(',');
        let tagSortMap = {};
        this.setState({visibleTags: tempTags});

        _.each(tempTags, (id, index) => {tagSortMap[id] = index});
        visibleComments = _.intersectionWith(tagList, tempTags, (a, b)=> {return a.id === b;});
        _.each(visibleComments, (val) => {val.sortIndex = tagSortMap[val.id]});

        let sorted = _.sortBy(visibleComments, obj => obj.sortIndex);
        this.setState({dsVisibleCommments : this.state.ds.cloneWithRows(sorted)});
    }

    getColor(){
        return 'skyblue';
    }
    renderCommentRow(row){
        let indicatorStyle = Utils.getBorderColor(row.commentType);

        if (row === undefined){
            return (<View><Text>No comments to show.</Text></View>);
        }
        return (<View style={styles.commentListItemContainer}>
            <View style={[styles.commentTypeIndicator, indicatorStyle]}></View>
            <View style={styles.commentData}>
                <View><Text style={{color: '#95A5A6'}}>{row.createdBy} - {Utils.getTimeElapsed(row.createdDate)}</Text></View>
                <Text>{row.comment}</Text>
            </View>
            </View>);
    };

    writeComment(data) {
        console.log('start writing comment:', data);

        // reset data for new comment
        this.setState({selectedCommentType: COMMENT_TYPE.PUBLIC});
        this.setState({comment: ''});
        this.setState({tagMode: TAG_MODE.WRITE});
        this.setState({commentingData : data});
    }

    onPressPostButton(){
        let url = 'http://10.10.10.121:8707/hvr/api/comment';
        // let url = 'http://192.168.3.238:8707/hvr/api/comment';
        let postData = {
            authorId: this.props.route.passProps.authorId,
            articleTitle: this.state.ogTitle,
            articleUrl: !!this.state.commentingData.articleUrl ? this.state.commentingData.articleUrl : null,
            comment: this.state.comment,
            commentType: this.state.selectedCommentType,
            elementFingerprint: this.state.commentingData.hash,
            context: 'CONTEXTTEXT',
            hash: this.state.commentingData.hash,
            paragraphClip: this.state.commentingData.textContent.substring(0, 150),
            siteUrl: this.state.siteUrl,
            group:{id: null, name: null},
        };

        console.log('comment data: ', JSON.stringify(postData));
        fetch(url,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + this.props.route.passProps.bearer,
                },
                body: JSON.stringify(postData),
            }
        ).then((response) => {
            console.log('*** first response : ', response);
            return response.json();
        }).then((responseJson) => {
            console.log('responseJson: ', responseJson);
            this.updateCommentList();
            this.onPressCancelWriteComment();

        }).catch((error) => {
            console.log('****** Error ***** ');
           console.error(error);
        });
    }

    updateCommentList(){
        console.log(' updateCommentList');
        // 1. get tags from service
        session.getComments(this.state.siteUrl)
            .then((response) => {
            // console.log('**** response: ', response);
            return response.json();
        })
            .then((responseJson) => {
                console.log('responseJson: ', responseJson);
                let webViewTagList = this.convertToWebViewTagList(responseJson);

                this.setState({tagList: responseJson});
                let setTagListString = 'setTagList(' +  JSON.stringify(webViewTagList) + ');';
                console.log('setTagListString comment: ', setTagListString);
                this.refs.webviewbridge.runJavaScript(setTagListString);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    setPageData(data){
        this.setState({siteUrl: data.siteUrl});
        this.setState({ogTitle: data.ogTitle});

        console.log('siteUrl: ' + data.siteUrl);
        console.log('ogTitle: ' + data.ogTitle);
    }

    onBridgeMessage(message){
        const { webviewbridge } = this.refs;
        // console.log('onBridgeMessage:', message);
        try{
            let wvbAction = JSON.parse(message);
            // console.log('wvbAction: ', wvbAction);
            if (wvbAction.action){
                if (wvbAction.action === HVR_ACTION.SHOW_VISIBLE_TAGS){
                    this.showComments(wvbAction.data);
                }else if (wvbAction.action === HVR_ACTION.DEBUG){
                    console.log(wvbAction.data);
                }else if (wvbAction.action === HVR_ACTION.UPDATE_TAGLIST){
                    this.updateCommentList();
                }else if (wvbAction.action === HVR_ACTION.GET_TEXT_HASH) {
                    this.sendTextHashToWebView(wvbAction.data);
                }else if (wvbAction.action === HVR_ACTION.GET_IMAGE_HASH){
                    this.sendImageHashToWebView(wvbAction.data);
                }else if (wvbAction.action === HVR_ACTION.WRITE_COMMENT){
                    this.writeComment(wvbAction.data);
                }else if (wvbAction.action === HVR_ACTION.SET_PAGE_DATA){
                    this.setPageData(wvbAction.data)
                }
            }

        }catch(e){
            console.log(e + 'error receiving wvbAction:', message);
        }
    }
    onPressCancelWriteComment(){
        console.log('cancel write');
        this.setState({tagMode: TAG_MODE.IDLE});
        this.exitHuvrTagMode();
    }

    _renderCommentList(){
        if (this.state.isShowComments){
            return (<View style={styles.visibleCommentsList}>
                <ListView dataSource={this.state.dsVisibleCommments}
                          renderRow={this.renderCommentRow}
                          showsVerticalScrollIndicator={false}
                          enableEmptySections={true}/>
            </View>);
        }
    }

    _renderInputContainer(){
        if (this.state.tagMode === TAG_MODE.WRITE){
            return (<View style={styles.inputContainerBackground}>
                <View style={styles.inputMask}>
                </View>
                <View style={styles.inputContainer}>

                    <TouchableHighlight style={styles.cancelWriteCommentButton} activeOpacity={0.05}
                                        onPress={this.onPressCancelWriteComment.bind(this)}><Text>X</Text>
                    </TouchableHighlight>
                    <TextInput multiline={true} style={styles.commentInput} placeholder='Write comment here.' placeholderTextColor='#7F8C8D' autoFocus={true}
                        onChangeText={(text) => this.setState({comment: text}) }>
                    </TextInput>
                    <View style={styles.commentTypeContainer}>
                        <TouchableWithoutFeedback onPress={this.onPressPublicButton.bind(this)}>
                            <View style={styles.commnetTypeButtonInnerContainer}>
                                <View style={[styles.radioButton, this.state.buttonPublicStyle]}></View>
                                <Text sylte={this.state.buttonPublicTextStyle}>Public</Text>
                            </View>
                        </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback onPress={this.onPressPrivateButton.bind(this)}>
                            <View style={styles.commnetTypeButtonInnerContainer}>
                                <View style={[styles.radioButton, this.state.buttonPrivateStyle]}></View>
                                <Text style={this.state.buttonPrivateTextStyle}>Private</Text>
                            </View>
                        </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback onPress={this.onPressGroupButton.bind(this)}>
                            <View style={styles.commnetTypeButtonInnerContainer}>
                                <View style={[styles.radioButton, this.state.buttonGroupStyle]}></View>
                                <Text style={this.state.buttonGroupTextStyle}>Group</Text>
                            </View>
                        </TouchableWithoutFeedback>

                        <TouchableWithoutFeedback onPress={this.onPressPostButton.bind(this)}>
                            <View style={styles.postButtonContainer}>
                                <Text style={styles.postButton}>Post</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </View>);
        }
    }
    onPressHvrAction(){
        console.log('onPressHvrAction');
        const {isOpenHvrAction, initRight, initBottom, dx, dy} = this.state;
        this.setState({isOpenHvrAction: !isOpenHvrAction});
        let newDx = (isOpenHvrAction ? 0 : dx);
        let newDy = (isOpenHvrAction ? 0 : dy);

        Animated.parallel([
            Animated.timing(this.state.searchRight, {toValue: initRight + newDx}),
            Animated.timing(this.state.searchBottom, {toValue: initBottom + newDy}),

            Animated.timing(this.state.eyeRight, {toValue: initRight - newDx}),
            Animated.timing(this.state.eyeBottom, {toValue: initBottom + newDy}),

            Animated.timing(this.state.messageRight, {toValue: initRight + newDx}),
            Animated.timing(this.state.messageBottom, {toValue: initBottom - newDy}),

            Animated.timing(this.state.newTagRight, {toValue: initRight - newDx}),
            Animated.timing(this.state.newTagBottom, {toValue: initBottom - newDy}),
        ]).start();
    }

    _renderHvrButton() {
        if (!this.canShowButtons()){
            return null;
        }
        return (<View style={styles.hvrActionButtonContainer}>
            <View
                style={styles.ButtonRadialContainer}>
                <TouchableWithoutFeedback
                    onPress={this.onPressHvrAction.bind(this)}>
                    <Image style={styles.ButtonRadial}
                           source={images.buttons.radial}/>
                </TouchableWithoutFeedback>
            </View>
        </View>);
    }

    _renderHideButton() {
        if (!this.canShowButtons()){
            return null;
        }
        return (<Animated.View
            style={[styles.ButtonSubRadialContainer, {right: this.state.eyeRight, bottom: this.state.eyeBottom}]}>
            <Image style={styles.hvrActionButtonSubRadial}
                   source={images.buttons.eye}/>
        </Animated.View>);
    }

    _renderTagButton() {
        if (!this.canShowButtons()){
            return null;
        }
        return (<Animated.View
            style={[styles.ButtonSubRadialContainer,
                {right: this.state.newTagRight, bottom: this.state.newTagBottom},
            ]}>
            <TouchableWithoutFeedback
                onPress={this.onPressTagButton.bind(this)}>
                <Image style={styles.hvrActionButtonSubRadial}
                       source={images.buttons.newPin}/>
            </TouchableWithoutFeedback>
        </Animated.View>);
    }

    _renderSearchButton() {
        if (!this.canShowButtons()){
            return null;
        }
        return ( <Animated.View
            style={[styles.ButtonSubRadialContainer,
                {right: this.state.searchRight, bottom: this.state.searchBottom},
            ]}>
            <TouchableWithoutFeedback
                onPress={()=>{}}>
                <Image style={styles.hvrActionButtonSubRadial}
                       source={images.buttons.search}/>
            </TouchableWithoutFeedback>
        </Animated.View>);
    }

    _renderToggleCommentsButton() {
        if (!this.canShowButtons()){
            return null;
        }
        return (<Animated.View
            style={[styles.ButtonSubRadialContainer, {right: this.state.messageRight, bottom: this.state.messageBottom}]}>
            <TouchableWithoutFeedback
                onPress={this.toggleComments.bind(this)}>
                <Image style={styles.hvrActionButtonSubRadial}
                       source={images.buttons.message}/>
            </TouchableWithoutFeedback>
        </Animated.View>);
    }


    canShowButtons() {
        var isAllowed = this.state.tagMode === TAG_MODE.IDLE;
            // && this.state.url.length > 0;
        // console.log('canShowButtons: ', isAllowed);
        return isAllowed;
    }

    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState({renderPlaceholderOnly: false});
        });
    }
    render() {
        let that = this;

        function updateUrl(text){
            console.log('updating url:', text);
            if (text && text.length > 0){
                that.setState({url: text});
            }
        }

        function onLoad(event){
            console.log('onload');
            console.log('url: ', that.state.url);
            if (that.state.url.length > 0){
                that.refs.webviewbridge.injectJavaScript();
            }
        }

        function onError(){
            console.log('onError');
        }

        function resetUrlInput(){
            console.log('resetUrlInput');
            that.setState({urlInput: that.state.url});
        }


        if (this.state.renderPlaceholderOnly) {
            return (<View>
                <Text>Loading...</Text>
            </View>);
        }

        return (
            <View style={styles.container}>

                <View style={styles.topNav}>
                    <TouchableHighlight
                        onPress={() => {}}
                        activeOpacity={75 / 100}
                        underlayColor={"rgb(210,210,210)"}>
                        <Image style={styles.topNavItem} source={images.buttons.profile}/>
                    </TouchableHighlight>
                    <TextInput value={this.state.urlInput}
                               style={styles.urlInput}
                               onChangeText={(urlInput) => this.setState({urlInput})}
                               onBlur={()=> resetUrlInput()}
                               keyboardType="url"
                               onSubmitEditing={(event) => updateUrl(event.nativeEvent.text)}/>
                </View>

                <WebViewBridge
                    ref="webviewbridge"
                    onBridgeMessage={this.onBridgeMessage.bind(this)}
                    javaScriptEnabled={true}
                    source={{uri: this.state.url}}
                    onLoad={onLoad}
                    onError={onError}
                    style={styles.webcontainer}/>

                {this._renderCommentList()}
                {this._renderInputContainer()}

                {this._renderHvrButton()}
                {this._renderHideButton()}
                {this._renderTagButton()}
                {this._renderSearchButton()}
                {this._renderToggleCommentsButton()}

            </View>
        )
    }
}

export default Main