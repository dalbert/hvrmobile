import React from "react";
import {StyleSheet} from "react-native";
import {COLORS, GLOBALCONSTANTS} from "hvrMobile/app/common/Constants";


var loginStyles = StyleSheet.create({
    logo: {
        height: 130,
        resizeMode: 'contain',
    },
    slogan: {
        alignSelf: 'center',
        color: COLORS.WHITE,
        fontSize: 18
    },

    loginButtonContainer: {
        backgroundColor: COLORS.WHITE,
        padding: 15,
        borderRadius: GLOBALCONSTANTS.BORDERRADIUS
    },

    loginButtonText: {
        color: COLORS.PUBLIC,
        textAlign: 'center',
        fontSize: 14
    },
    loginActions: {
        color: COLORS.WHITE
    },
    signUpLink: {
        textDecorationLine: 'underline',
        marginLeft: 5,
    },
    forgetPassword: {
        textDecorationLine: 'underline',
    }

});

export default loginStyles;