import React, {Component} from "react";
import {Image, Keyboard, Text, TouchableOpacity, View} from "react-native";
import Main from "hvrMobile/app/scenes/Main/Main";
import SignUp from "hvrMobile/app/scenes/SignUp/SignUp";
import GLOBALSTYLES from "hvrMobile/app/common/globalStyles";
import {COLORS} from "hvrMobile/app/common/Constants";
import styles from "./styles";
import images from "hvrMobile/app/config/images";
import session from "hvrMobile/app/services/session";
import FormInput from "hvrMobile/app/components/FormInput";
const _ = require('lodash');

const MESSAGE = {
    EMAIL: {
        REQUIRED: 'This field is required',
    },
    PASSWORD: {
        REQUIRED: 'This field is required',
    }
};

class Login extends Component {

    static propTypes = {};
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            email: 'da@architech.ca',
            password: 'hvrhvrhvr',
            errorLogin: false,
            behavior: 'padding',
            emailMessage: '',
            passwordMessage: '',
            emailBorderColor: '#FFFFFF',
            passwordBorderColor: '#FFFFFF',
        };
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow() {
        this.setState({
            bottomPadding: {paddingBottom: 0}
        })
    }

    _keyboardDidHide() {
        this.setState({
            bottomPadding: {paddingBottom: 60}
        })
    }

    _renderLoginError() {
        if (this.state.errorLogin) {
            return <View><Text style={GLOBALSTYLES.errorText}>Invalid email and password. Please try
                again.</Text></View>
        } else {
            return null;
        }
    }

    _validateInputs() {
        let isValid = true;
        if (this.state.email.length === 0) {
            this.setState({emailMessage: MESSAGE.EMAIL.REQUIRED});
            this.setState({emailBorderColor: '#D0021B'});
            isValid = false;
        } else {

        }
        if (this.state.password.length === 0) {
            this.setState({passwordMessage: MESSAGE.PASSWORD.REQUIRED});
            this.setState({passwordBorderColor: '#D0021B'});
            isValid = false;
        } else {
            this.setState({passwordBorderColor: '#FFFFFF'});
        }

        return isValid;
    }

    _onPressLogin() {
        this.setState({errorLogin: false});

        if (!this._validateInputs()) {

        } else {
            session.login(this.state.email, this.state.password)
                .then((response) => {
                    console.log('response: ', response);
                    if (response.status !== 200) {
                        return {};
                    }
                    return response.json();
                })
            .then((responseJson) => {
                console.log('responseJson: ', responseJson);
                if (responseJson !== null && !!responseJson.userName) {
                    this.props.navigator.replace({
                        name: 'Main',
                        component: Main,
                        passProps: {authorId: responseJson.userId, bearer: responseJson.token}
                    });
                    session.setToken(responseJson.token);
                    session.setRefreshToken(responseJson.refreshToken);
                    session.setAuthorId(responseJson.userId);
                } else {
                    this.setState({errorLogin: true});
                }
            })
            .catch((error) => {
                console.error(error);
            });
        }
    }

    _signupSuccess() {
        console.log('success signup recieved on login page');
        this.setState({successfulSignup: true});
    }

    _onPressSignUp() {
        console.log('pushing singup');
        this.props.navigator.push({
            name: 'SignUp',
            component: SignUp,
            passProps: {
                signupSuccess: this._signupSuccess
            }
        });
    }
    _renderButton() {
        return (
            <TouchableOpacity onPress={() => this._onPressLogin()}>
                <View style={[GLOBALSTYLES.buttonPrimaryContainer, {marginTop: 8}]}>
                    <Text style={GLOBALSTYLES.buttonPrimaryText}>SIGN IN</Text>
                </View>
            </TouchableOpacity>
        );
    }

    _hasPasswordError() {
        return this.state.passwordMessage.length > 0;
    }

    _resetEmailValidation() {
        this.setState({emailMessage: ''});
    }

    _resetPasswordValidation() {
        this.setState({passwordMessage: ''});
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={[GLOBALSTYLES.background, {backgroundColor: '#299155'}]}>
                    <Image source={images.backgrounds.world} style={[GLOBALSTYLES.background,]}/>
                </View>
                <View style={[GLOBALSTYLES.mainContainer, GLOBALSTYLES.background]}>
                    <View style={[{zIndex: 103}, {flexDirection: 'column', flex: 1}]}>
                        <View style={[{flex: 1, justifyContent: 'center', alignItems: 'center'}]}>
                            <Image style={[styles.logo]}
                                   source={images.icons.logo.login}
                            />
                    </View>
                        <View style={{flex: 2}}>

                            {this._renderLoginError()}

                            <FormInput
                                keyboardType='email-address'
                                hasError={this.state.emailMessage.length > 0}
                                value={this.state.email}
                                placeholder='Email'
                                errorMessage={this.state.emailMessage}
                                onChange={() => this._resetEmailValidation()}
                                onChangeText={(email) => this.setState({email})}>
                            </FormInput>

                            <FormInput
                                hasError={this._hasPasswordError()}
                                value={this.state.password}
                                placeholder='Password'
                                isPassword={true}
                                errorMessage={this.state.passwordMessage}
                                onChange={() => this._resetPasswordValidation()}
                                onChangeText={(password) => this.setState({password})}
                            />


                            {this._renderButton()}
                            <View style={[{flexDirection: 'row', justifyContent: 'space-between', marginTop: 20}]}>
                                <Text onPress={() => this._onPressSignUp()} style={[styles.loginActions, {}]}>
                                    New Here? <Text style={styles.signUpLink}>Sign Up</Text>
                                </Text>
                                <Text style={[styles.loginActions, styles.forgetPassword]}>Forget Password?</Text>
                            </View>

                        </View>

                </View>

                </View>

</View>
        );
    }
}

export default Login