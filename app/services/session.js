import config from "../config/environment";

var _token = '';
var _refreshToken = '';
var _authorId = '';

let getHeaders = function (isAuth) {
    let _headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    };
    if (isAuth) {
        _headers['Authorization'] = 'Bearer ' + _token;
    }
    // console.log('_header: ', _headers);
    return _headers;
};

let getAuthHeaders = function () {
    return getHeaders(true)
};


const PATH = {
    SIGNIN: '/hvr/api/public/signin/',
    SIGNUP: '/hvr/api/public/register',
    COMMENT: '/hvr/api/comment',
    VALIDATE: {
        EMAIL: '/hvr/api/public/validate/email',
        USERNAME: '/hvr/api/public/validate/login',
    }
};

const session = {
    login: function (email, password) {
        return fetch(config.hvrServerUrl + PATH.SIGNIN,
            {
                method: 'POST',
                headers: getHeaders(),
                body: JSON.stringify({email: email, password: password})
            });
    },
    signup: function (email, username, password) {
        return fetch(config.hvrServerUrl + PATH.SIGNUP,
            {
                method: 'POST',
                headers: getHeaders(),
                body: JSON.stringify({
                    email: email,
                    login: username,
                    password: password,
                })
            }
        );
    },
    getComments: function (siteUrl) {
        console.log('getComments - authorId: ', _authorId);
        return fetch(config.hvrServerUrl + PATH.COMMENT
            + '?siteUrl=' + siteUrl + '&authorId=' + _authorId,
            {
                method: 'GET',
                headers: getAuthHeaders(),
            }
        );
    },
    // TODO: need a state to set these tokens and authorId
    setToken: function (token) {
        _token = token;
    },
    setRefreshToken: function (refreshToken) {
        _refreshToken = refreshToken;
    },
    setAuthorId: function (authorId) {
        _authorId = authorId;
    },
    validateEmail: function (email) {
        // http://localhost:8707/hvr/api/public/validate/email?email=da%40architech.ca
        console.log('session - validateEmail ' + email);
        return fetch(config.hvrServerUrl + PATH.VALIDATE.EMAIL + '?email=' + email,
            {
                method: 'POST',
            }
        );
    },
    validateUsername: function (username) {
        console.log('session - validate username');
        return fetch(config.hvrServerUrl + PATH.VALIDATE.USERNAME + '?login=' + username,
            {
                method: 'POST',
            }
        );

    }
};

export default session;