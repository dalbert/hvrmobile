'use strict';
import { PropTypes } from 'react';
import { NativeModules, requireNativeComponent, View } from 'react-native';

var iface = {
  name: 'ProgressBarAndroid',
  propTypes: {
  	progress: PropTypes.number,
    indeterminate: PropTypes.bool,
    ...View.propTypes // include the default view properties
  },
};

var ProgressBarAndroid = requireNativeComponent('ProgressBarAndroid', iface);

export default ProgressBarAndroid;