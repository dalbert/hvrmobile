import React, {Component} from "react";
import {Image, Text, TextInput, TouchableWithoutFeedback, View} from "react-native";
import GLOBALSTYLES from "hvrMobile/app/common/globalStyles";
import images from "hvrMobile/app/config/images";
import {COLORS, REGEX, STATUS} from "hvrMobile/app/common/Constants";

class FormInput extends Component {
    static propTypes = {};

    static defaultProps = {};

    constructor(props) {
        super(props);
        console.log('props: ', props);
        this.state = {
            hidePassword: true,
            password: '',
            hasFocus: false,
        };
    }

    handleVisibilityChange() {
        this.setState({hidePassword: !this.state.hidePassword});
    }

    _onChange() {
        this.props.onChange()
    }

    _onChangeText(value) {
        this.props.onChangeText(value);
    }

    _renderEyeIcon() {
        let source = images.icons.eye.hidden;
        if (!this.props.isPassword || this.props.isPassword === false) {
            return null;
        }
        if (!this.state.hidePassword) {
            source = images.icons.eye.shown;
        }
        return (
            <TouchableWithoutFeedback onPress={() => this.handleVisibilityChange()}>
                <Image style={[GLOBALSTYLES.passwordInputVisibility, {zIndex: 102}]} source={source}/>
            </TouchableWithoutFeedback>
        );
    }

    _renderErrorMessage() {
        if (!this.props.errorMessage || this.props.errorMessage.length === 0) {
            return null;
        }
        return (
            <View>
                <View style={[{flexDirection: 'row', justifyContent: 'flex-start'}]}>
                    <Text
                        style={[GLOBALSTYLES.errorMessageBox, {color: COLORS.WHITE},]}>{this.props.errorMessage}</Text>
                </View>
                <View style={[GLOBALSTYLES.errorMessageTop]}></View>
            </View>
        );
    }

    _renderBackground() {
        let backColor = '#FFFFFF';
        if (!this.state.hasFocus) {
            backColor = 'transparent';
        }
        return (
            <View style={[GLOBALSTYLES.background, {backgroundColor: backColor, opacity: 0.1, zIndex: 100}]}></View>)
    }

    render() {
        return (
            <View style={{}}>
                <View style={[GLOBALSTYLES.textInputContainer,]}>
                    {this._renderBackground()}
                    <TextInput
                        keyboardType={this.props.keyboardType}
                        style={[{flex: 1, color: COLORS.WHITE, zIndex: 102}]}
                        placeholderTextColor={COLORS.WHITE} underlineColorAndroid='transparent'
                        value={this.props.value} placeholder={this.props.placeholder}
                        secureTextEntry={this.props.isPassword && this.state.hidePassword}
                        onFocus={() => this.setState({hasFocus: true})}
                        onBlur={() => this.setState({hasFocus: false})}
                        onChange={() => this._onChange()}
                        onChangeText={(text) => this._onChangeText(text)}>
                    </TextInput>
                    {this._renderEyeIcon()}
                </View>
                {this._renderErrorMessage()}
            </View>

        );
    }
}

export default FormInput;