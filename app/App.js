/* global XMLHttpRequest */

import React, { Component } from 'react';
import {
	StyleSheet,
	Navigator,
	View,
} from 'react-native';
import Login from 'hvrMobile/app/scenes/Login/Login';

// TODO: need to add "redux" and "deep-freeze" (for use with redux, to make sure code is free of mutations for state)

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#eee',
	},
});

const routeStack = [
	{ name: 'Login', component: Login },
];

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			initialRoute: null,
		};
	}

	componentDidMount() {
		// Waits for the redux store to be populated with the previously saved state,
		// then it will try to auto-login the user.
// 		const unsubscribe = store.subscribe(() => {
// 			if (store.getState().services.persist.isHydrated) {
// 				unsubscribe();
// 				this.autoLogin();
// 			}
// 		});
      this.setState({initialRoute: routeStack[0]});
      // this.setState({initialRoute: routeStack[1]});
	}

// 	autoLogin() {
// 		session.refreshToken().then(() => {
// 			this.setState({ initialRoute: routeStack[3] });
// 		}).catch(() => {
// 			this.setState({ initialRoute: routeStack[0] });
// 		});
// 	}

	renderContent() {
// 		if (!this.state.initialRoute) {
// 			return <Splash />;
// 		}

		return (
			<Navigator
				initialRoute={this.state.initialRoute}
				initialRouteStack={routeStack}
				configureScene={() => Navigator.SceneConfigs.HorizontalSwipeJump}
				renderScene={(route, navigator) =>
					<route.component route={route} navigator={navigator} {...route.passProps} />
				}
			/>
		);
	}

	render() {
		return (
			<View style={styles.container}>
					{this.renderContent()}
			</View>
		);
	}
}

export default App;
