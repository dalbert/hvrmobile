package com.hvrmobile;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.webkit.WebView;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.views.webview.ReactWebViewManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import javax.annotation.Nullable;

public class WebViewBridgeManager extends ReactWebViewManager {
    private static final String REACT_CLASS = "RCTWebViewBridge";
    private static final String TAG = WebViewBridgeManager.class.getSimpleName();

    public static final int COMMAND_SEND_TO_BRIDGE = 101;
    public static final int COMMAND_INJECT_JAVASCRIPT = 102;
    private static final int COMMAND_RUN_JAVASCRIPT = 103;

    private static final String INJECT_CSS_MAIN = "default.css";
    private static final String INJECT_JS_STYLE = "injectStyle.js";
    private static final String INJECT_JS_TAGS = "injectTags.js";

    private static final String PLACEHOLDER_STYLE = "{{STYLE}}";

    private Context mContext;

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public
    @Nullable
    Map<String, Integer> getCommandsMap() {
        Map<String, Integer> commandsMap = super.getCommandsMap();

        commandsMap.put("sendToBridge", COMMAND_SEND_TO_BRIDGE);
        commandsMap.put("injectJavaScript", COMMAND_INJECT_JAVASCRIPT);
        commandsMap.put("runJavaScript", COMMAND_RUN_JAVASCRIPT);

        return commandsMap;
    }

    private String readText(String filename, Context context){
        AssetManager am = context.getAssets();
        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is = am.open(filename);
            reader = new BufferedReader(new InputStreamReader(is));
            String mLine ;
            while ((mLine = reader.readLine()) != null){
//                if (!mLine.matches("^\\s*//")){
                    stringBuilder.append(mLine).append(' ');
//                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    private void loadStyle(String cssFile, WebView root, Context reactContext) {
        mContext = reactContext;
        String contentInjecter = readText(INJECT_JS_STYLE, reactContext);
        String styleContent = readText(cssFile, reactContext);

        String newScript = contentInjecter.replace(PLACEHOLDER_STYLE, styleContent);

        evaluateJavascript(root, newScript);
        Log.d(TAG, "**** fileread: styleContent: " + newScript);
    }

    private void loadJavascript(String jsFile, WebView root, Context reactContext) {
        String jsContent = readText(jsFile, reactContext);
        evaluateJavascript(root, jsContent);
        Log.d(TAG, "**** fileread: jsContent: " + jsContent);
    }

    private void loadAllJavascript(WebView root, Context reactContext) {
        loadStyle(INJECT_CSS_MAIN, root, reactContext);
        loadJavascript(INJECT_JS_TAGS, root, reactContext);
    }

    @Override
    protected WebView createViewInstance(ThemedReactContext reactContext) {
//        Log.d(TAG, "********** default.css asdfasdf 00");
        WebView root = super.createViewInstance(reactContext);
        root.addJavascriptInterface(new JavascriptBridge(root), "WebViewBridge");

//        Log.d(TAG, "******** asdfasdf default.css 01");

        loadAllJavascript(root, reactContext);

        return root;
    }

    @Override
    public void receiveCommand(WebView root, int commandId, @Nullable ReadableArray args) {
        super.receiveCommand(root, commandId, args);

        switch (commandId) {
            case COMMAND_SEND_TO_BRIDGE:
                sendToBridge(root, args.getString(0));
                break;
            case COMMAND_INJECT_JAVASCRIPT:
                loadAllJavascript(root, mContext);
                break;
            case COMMAND_RUN_JAVASCRIPT:
                runJavascript(root, args.getString(0));
                break;
            default:
                //do nothing!!!!
        }
    }

    private void sendToBridge(WebView root, String message) {
        String script = "WebViewBridge. onMessage('" + message + "');";
        WebViewBridgeManager.evaluateJavascript(root, script);
    }

    private void runJavascript(WebView root, String message){
        WebViewBridgeManager.evaluateJavascript(root, message);
    }

    static private void evaluateJavascript(WebView root, String javascript) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            root.evaluateJavascript(javascript, null);
        } else {
            root.loadUrl("javascript:" + javascript);
        }
    }

    @ReactProp(name = "allowFileAccessFromFileURLs")
    public void setAllowFileAccessFromFileURLs(WebView root, boolean allows) {
        root.getSettings().setAllowFileAccessFromFileURLs(allows);
    }
}
