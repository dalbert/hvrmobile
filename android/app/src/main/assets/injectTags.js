﻿console.log('**** injectTags 00');

var HVR_TAG = 'hvr-tag';
var HVR_ACTION = {
    SHOW_VISIBLE_TAGS: "showVisibleTags",
    DEBUG: "Debug",
    UPDATE_TAG_LIST: "updateTagList",
    GET_TEXT_HASH: 'getTextHash',
    GET_IMAGE_HASH: 'getImageHash',
    WRITE_COMMENT: 'writeComment',
    SET_PAGE_DATA: 'setPageData',
};

var DATA_ATTRIBUTES = {
    TAG_ID: 'data-tagid',
};

var TAG_MODE = {
    IDLE: 0,
    SELECT: 1,
    WRITE: 2,
};

var textElementsNames = 'p,a';
var imageElementsNames = 'img';

var justClicked = false;
var shownTags = [];
var webViewTagMap = {};

var textMap = {
    totalCount: 0,
    receivedCount: 0,
    mapping: {}
};
var imageMap = {
    totalCount: 0,
    receivedCount: 0,
    mapping: {}
};

var tagMode = TAG_MODE.IDLE;

var requestTextHash = function(text){
    var obj = {action: HVR_ACTION.GET_TEXT_HASH, data: text};
    WebViewBridge.send(JSON.stringify(obj));
};

var requestImageHash = function(text){
    var obj = {action: HVR_ACTION.GET_IMAGE_HASH, data: text};
    WebViewBridge.send(JSON.stringify(obj));
};

var testComment = function(val){
    var obj = { action: HVR_ACTION.DEBUG, data: val};
    WebViewBridge.send(JSON.stringify(obj));
};

var findVisibleTags = function(){
    var tags = document.querySelectorAll('.' + HVR_TAG);
    var visibleTags = [];
    for(var i=0; i < tags.length; i++){
        if (isElementVisible(tags[i], false)){
            visibleTags.push(tags[i]);
        }
    }

    return visibleTags;
};

var getClassForTags = function(tagMap){
    var tagClass = HVR_TAG;

    var tagMapKeys = Object.keys(tagMap);
    var isSingle = tagMapKeys.length === 1;
    var isMixed = false;

    var tempType = tagMap[tagMapKeys[0]].commentType;
    console.log('tempType: ' + tempType);
    for (var i=1; i < tagMapKeys.length; i++){
        if (tempType !== (tagMap[tagMapKeys[i]]).commentType){
            console.log('commentType: ' + (tagMap[tagMapKeys[i]]).commentType);
            isMixed = true;
            break;
        }
    }
    if (isSingle){
        tagClass += '-single';
    }else{
        tagClass += '-multiple';
    }
    console.log('isMixed: ' + isMixed);
    if (isMixed){
        tagClass += '-mixed';
    }else{
        tagClass += '-' + tempType.toLowerCase();
    }
    console.log('tagClass: ' +  tagClass);
    return tagClass;
};

var getTagIds = function(tagMap){
    var tagMapKeys = Object.keys(tagMap);
    var tagIdList = [];
    for (var i=0; i < tagMapKeys.length; i++){
        tagIdList.push((tagMap[tagMapKeys[i]]).id);
    }
    return tagIdList.join(',');
};

var addTagsToPage = function(){
    console.log('addtagstopage');
    /* find elements and add div tags */
     var objectKeys = Object.keys(textMap.mapping);
     for (var i=0; i < objectKeys.length; i++){

        var contentObject = textMap.mapping[objectKeys[i]];
        var tagMapForHash = webViewTagMap[contentObject.hash];

           if (!!tagMapForHash){
               var tagClass = getClassForTags(tagMapForHash);
               var newTag = document.createElement('DIV');
               newTag.setAttribute('class',  HVR_TAG + ' ' + tagClass);
               newTag.setAttribute(DATA_ATTRIBUTES.TAG_ID, getTagIds(tagMapForHash));
               console.log('classAttribute : ' + newTag.getAttribute('class'));
               var tagTarget = contentObject.node;
               tagTarget.insertBefore(newTag, tagTarget.firstChild);
           }else{
               /*console.log('did not find taghash for ' + objectKeys[i]);*/
           }
       }
};

var setTagList = function(newTagList){
    console.log('doing  setTagList');
    webViewTagMap = {};
    if (newTagList){
        for (var i=0; i < newTagList.length; i++){
            var tokens = newTagList[i].split('::');
            var id = tokens[0];
            var hash = tokens[1];
            var commentType = tokens[2];

            var newTagObject = {
                id: id,
                commentType: commentType,
            };
            if (webViewTagMap[hash] === undefined){
                webViewTagMap[hash] = {id: newTagObject};
            }else{
                (webViewTagMap[hash])[id] = newTagObject;
            }
        }
    }
    addTagsToPage();
};

var getOffsetTop = function(element){
    if (!!element){

        var rect = element.getBoundingClientRect();

        var doc = element.ownerDocument;
        var win = getWindow(doc);
        var docElem = doc.documentElement;

        return rect.top + win.pageYOffset - docElem.clientTop;
    }
};

var getWindow = function(elem){
    return isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView;
};

var isWindow = function(obj){
    return obj != null && obj === obj.window;
};

var isElementVisible = function(element, requireFullInView){
    var pageTop = window.pageYOffset;
    var pageBottom = pageTop + window.innerHeight;
    var elementTop = getOffsetTop(element);
    var elementBottom = elementTop + element.offsetHeight;

    var windowHeight = window.innerHeight;

    if (requireFullInView) {
        return ((pageTop < elementTop) && (pageBottom > elementBottom));
    } else {
        return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
    }
};

function doShowTags(){
    shownTags = findVisibleTags();
    var data = {action: HVR_ACTION.SHOW_VISIBLE_TAGS, data: null};
    var tagIds = [];
    for(var i=0; i < shownTags.length; i++){
        tagIds.push(shownTags[i].getAttribute(DATA_ATTRIBUTES.TAG_ID));
    }
    data.data = tagIds.join(',');
    WebViewBridge.send(JSON.stringify(data));
}

var myTimeout = null;

window.onscroll = function(){
    if (myTimeout !== null){
        clearTimeout(myTimeout);
    }
    myTimeout = setTimeout(doShowTags, 1);
};
var setupClickEvents = function(mapWrapper){
    var mapping = mapWrapper.mapping;
    var mapKeys = Object.keys(mapping);

    for (var i=0; i < mapKeys.length; i++){
        var el = mapping[mapKeys[i]].node;
        el.onclick = function(e){
            if (justClicked){
                return;
            }else{
                justClicked = true;
                setTimeout(function(){
                    justClicked = false;
                }, 500);
                console.log('tagMode: ' + tagMode);
                if (tagMode === TAG_MODE.SELECT && e.target !== document.body){
                    testComment('tagMode click');

                    removeCssClass(document.querySelector('.hover-outline'), 'hover-outline');
                    addCssClass(e.target, 'hover-outline');

                    /* send target data to backend*/
                    writeTagComment(e.target);
                    /* if (e.target.tagName.toUpperCase() === 'IMG'){
                        writeTagComment(e.target, true);
                    }*/

                    e.preventDefault();
                }
            }
        };
    }

};

var getContentObject = function(node){
    var mapAllElements = textMap.mapping;
    var objectKeys = Object.keys(mapAllElements);
    var foundNode = null;
    for (var i=0; i < objectKeys.length; i++){
        var currentContentObject = mapAllElements[objectKeys[i]];
        if (currentContentObject.node === node){
            foundNode = currentContentObject;
            console.log('found Node: ' + currentContentObject.node.textContent);
            break;
        }
    }
    return foundNode;
};

function writeTagComment(element){
    if (element == null) return;

    var contentObject = getContentObject(element);
    window.scroll(0, getOffsetTop(element));
    var tagData = {
        id: contentObject.id,
        textContent: contentObject.node.textContent,
        hash: contentObject.hash,
    };
    if (contentObject.node.tagName.toUpperCase() === 'A'){
        tagData.articleUrl = a.getAttribute('href');
    }
    var data = {action: HVR_ACTION.WRITE_COMMENT, data: tagData};
    WebViewBridge.send(JSON.stringify(data));
}

function addCssClass(element, cssClass){
    if (element == null) return;

    var currentClass = element.getAttribute('class');
    element.setAttribute('class', currentClass + ' ' + cssClass);
}

function removeCssClass(element, cssClass){
    if (element == null) return;

    var currentClass = element.getAttribute('class');
    var newClass = [];
    if (currentClass.length > 0){
        var currentClassList = currentClass.split(' ');
        for (var i=0; i < currentClassList.length; i++){
            var tempClass = currentClassList[i];
            if (tempClass != cssClass){
                newClass.push(tempClass);
            }
        }
        element.setAttribute('class', newClass.join(' '));
    }
}

function enterHuvrTagMode(){
    testComment('from webview: entering tag mode');
    tagMode = TAG_MODE.SELECT;
}

function exitHuvrTagMode(){
    testComment('from webview: exiting tag mode');
    removeCssClass(document.querySelector('.hover-outline'), 'hover-outline');
    tagMode = TAG_MODE.IDLE;
}

function hasImageDescendent(el){
    return !!el.querySelector('img');
}

function initializeTextHash(){
    var commentableTextElements = document.querySelectorAll(textElementsNames);

    textMap.receivedCount = 0;
    textMap.totalCount = 0;
    textMap.mapping = {};

    for (var i=0; i < commentableTextElements.length; i++){
        var el = commentableTextElements[i];
        var textContent = el.textContent;

        if (textContent.length > 0 && !hasImageDescendent(el)){
            textMap.mapping[textMap.totalCount] = {
                id: textMap.totalCount,
                node: commentableTextElements[i],
                hash: null,
            };
            requestTextHash(textMap.totalCount + '|' + encodeURIComponent(textContent));
            textMap.totalCount++;
        }
    }
    setupClickEvents(textMap);
    /* should timeout after a bit, to make sure tags are settled */
}


function initializeImageHash(){

    var commentableImageElements = document.querySelectorAll(imageElementsNames);

    imageMap.receivedCount = 0;
    imageMap.totalCount = 0;
    imageMap.mapping = {};

    for (var i=0; i < commentableImageElements.length; i++){
        var el = commentableImageElements[i];
        var imageSrc = el.getAttribute('src');

        if (imageSrc.length > 0){
            imageMap.mapping[textMap.totalCount] = {
                id: imageMap.totalCount,
                node: commentableImageElements[i],
                hash: null,
            };
            requestImageHash(imageMap.totalCount + '|' + encodeURIComponent(imageSrc));
            imageMap.totalCount++;
        }
    }
    setupClickEvents(imageMap, true);
    /* should timeout after a bit, to make sure tags are settled */
}

function setTextHash(id, hash){
    /* testComment('received[' + id + '] = ' + hash); */
    var textContentHashObject = textMap.mapping[id];

    if (textContentHashObject && textContentHashObject.hash === null){
        textMap.receivedCount++;
        textContentHashObject.hash = hash;
    }
    if (textMap.receivedCount === textMap.totalCount){
        console.log('recieved all for text');
        checkPageHashReady();
    }else{
     /*   console.log('not yet received all (' + textMap.receivedCount + '/' + textMap.totalCount + ')'); */
    }
}

function setImageHash(id, hash){
    testComment('img received[' + id + '] = ' + hash);
    var contentHashObject = imageMap.mapping[id];

    if (contentHashObject && contentHashObject.hash === null){
        imageMap.receivedCount++;
        contentHashObject.hash = hash;
    }
    if (imageMap.receivedCount === imageMap.totalCount){
        console.log('recieved all for image');
        checkPageHashReady();
    }else{
        console.log('not yet received all (' + imageMap.receivedCount + '/' + imageMap.totalCount + ')');
    }
}

function checkPageHashReady(){
    if (textMap.receivedCount === textMap.totalCount
/*            && imageMap.receivedCount === imageMap.totalCount){ */
    ){
        WebViewBridge.send(JSON.stringify({action: HVR_ACTION.UPDATE_TAG_LIST, data: ''}));
    }
}

function setPageData(){
    var metaTags = document.querySelectorAll('meta');
    var ogTitle = '';
    var siteUrl = window.location.href;
    for (var i=0; i < metaTags.length; i++){
        if (metaTags[i].getAttribute('property') === 'og:title'){
            ogTitle = metaTags[i].getAttribute('content');
            break;
        }
    }
    var data = {
        ogTitle: ogTitle,
        siteUrl: siteUrl,
    };

    var obj = {action: HVR_ACTION.SET_PAGE_DATA, data: data};

    WebViewBridge.send(JSON.stringify(obj));

}

setPageData();
initializeTextHash();
/*initializeImageHash();*/
