﻿{
	var myCssId = 'hvr-webview-default-css';

	if (!document.getElementById(myCssId))
	{
		var head = document.getElementsByTagName('head')[0];
		var newStyle = document.createElement('style');
		newStyle.innerHTML = '{{STYLE}}';
		newStyle.setAttribute('id', myCssId);
		newStyle.setAttribute('type', 'text/css');
		head.appendChild(newStyle);

	}
}